#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <poll.h>

#include <stateless.h>

void usage() {
  puts("Usage:");
  puts("    -h  This help");
  puts("    -a  host4 address");
  puts("    -p  publish domain");
}

int main(int _argc, char** _argv) {
  char* addr = NULL;
  char* echo = NULL;
  int option;

  while( (option = getopt(_argc, _argv, "ha:p:")) != -1 ) {
    switch(option) {
    case 'h':
      usage();
      return EXIT_SUCCESS;
    case 'a':
      addr = optarg;
      break;
    case 'p':
      echo = optarg;
      break;
    default:
      usage();
      return EXIT_FAILURE;
    }
  }

  if (!addr || !echo) {
    printf("A host address and publish domain are required\n");
    usage();
    return EXIT_FAILURE;
  }

  /* Init the server */
  StatelessServerConfig ssc;
  ssc.echo_domain = echo;
  ssc.nameserver = NULL;
  ssc.use_ipv4 = 1;
  ssc.use_ipv6 = 0;

  int error;
  StatelessServer* server = stateless_server_new(&ssc, NULL, NULL, &error);
  if (!server) {
    printf("Can't create the server\n");
    return EXIT_FAILURE;
  }

  /* Add interface */
  StatelessStatus status = stateless_server_interface_add(server, addr);
  if (status != STATELESS_STATUS_OK) {
    printf("%s\n", stateless_status_str(status));
    stateless_server_free(&server);
    return EXIT_FAILURE;
  }

  /* StatelessStatus status; */
  char record[256];
  snprintf(record, sizeof(record), "testinstance._testtype._testscope.%s TXT \"This is an example TXT resource record offered by your SNS.\"", echo, addr);
  status = stateless_server_record_add(server, record);
  if (status != STATELESS_STATUS_OK) {
    printf("%s\n", stateless_status_str(status));
    stateless_server_free(&server);
    return EXIT_FAILURE;
  }

  /* poll */
  struct pollfd fds[1];
  fds[0].fd = stateless_get_udp_ipv4(server);
  fds[0].events = POLLIN;
  int ret = -1;
  while (poll(fds, 1, -1) >= 0) {
    stateless_event(server, fds[0].fd);
  }

  /* poll failed (Exit with SIGTERM ) */
  return EXIT_FAILURE;
}
