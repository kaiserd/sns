Scope Name Server
===

As of yet, the SNS implementation is just an example taken from our [ libmlsd ](https://gitlab.com/holst/mlsd). 
In its current state, it demonstrates establishing itself as an SNS for an example scope providing NS
resource records in the network's DNS cache.
It also answers when asked for an example TXT resource record that is under its authority.


### Build

Clone the project including the subrepos

```bash
git clone --recursive https://gitlab.com/kaiserd/sns.git
```

Build the SNS as well as its dependencies

```bash
cd sns
make
```

Delete all files generated while building the SNS

```bash
make clean

```

Delete all files generated while building the SNS *and* while building the dependencies

```bash
make distclean 
```

Optionally, give the SNS the necessary capabilities to run it as a user

```bash
make cap
```


### Test the SNS

The following command establishes an SNS using the interface with the IP address `192.168.1.10` and our testing service discovery domain `ssdisc.com`
for which our [perl reflector] (https://gitlab.com/kaiserd/mini_reflector) is the authoritative name server.

```
./sns -a 192.168.1.10 -p ssdisc.com
```

The SNS establishes itself as SNS for `_testtype._testscope.ssdisc.com` which you can verify by running


```bash
dig _testtype._testscope.ssdisc.com
```

The output should contain

```
;; QUESTION SECTION:
;_testtype._testscope.ssdisc.com. IN	A

;; ANSWER SECTION:
_testtype._testscope.ssdisc.com. 0 IN	A	192.168.1.10

;; AUTHORITY SECTION:
_testtype._testscope.ssdisc.com. 72 IN	NS	ns1._testtype._testscope.ssdisc.com.

;; ADDITIONAL SECTION:
ns1._testtype._testscope.ssdisc.com. 72	IN A	192.168.1.10

```

You can also ask for an example TXT record

```bash
dig TXT testinstance._testtype._testscope.ssdisc.com
```

which should yield

```bash
;; QUESTION SECTION:
;testinstance._testtype._testscope.ssdisc.com. IN TXT

;; ANSWER SECTION:
testinstance._testtype._testscope.ssdisc.com. 3566 IN TXT "This is an example TXT resource record offered by your SNS."

;; AUTHORITY SECTION:
_testtype._testscope.ssdisc.com. 66 IN	NS	ns1._testtype._testscope.ssdisc.com.

;; ADDITIONAL SECTION:
ns1._testtype._testscope.ssdisc.com. 66	IN A	192.168.1.10
```


This TXT record is provided by the SNS running on your device as your SNS is now (locally) authoritative
for `_testtype._testscope.ssdisc.com`. 
Anyone using the same DNS cache can now retrieve this TXT record using standard DNS methods.

