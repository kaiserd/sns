CC=gcc
CFLAGS= -g -I $(MLSDPATH)/src $(OPTFLAGS)
LDFLAGS=-Wl,-rpath=$(shell cd deps/mlsd && pwd) -L $(MLSDPATH) -l mlsd $(OPTLIBS)

MLSDPATH=deps/mlsd

SOURCES := $(wildcard src/*.c src/**/*.c)
OBJECTS := $(patsubst src/%.c, build/%.o, $(SOURCES))

TARGET=sns

.PHONY: all sudo clean

all: $(TARGET)

$(TARGET): libmlsd $(OBJECTS) Makefile
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJECTS) -o $@

build/%.o: src/%.c | builddir
	$(CC) $(CFLAGS) -c $< -o $@

libmlsd:
	@$(MAKE) -C $(MLSDPATH) 

builddir:
	mkdir -p build

cap: $(TARGET)
	sudo /sbin/setcap 'cap_net_bind_service=+ep' $(TARGET)


clean:
	@rm -rfv build
	@rm -rfv $(TARGET)

distclean:
	@rm -rfv build
	@rm -rfv $(TARGET)
	@$(MAKE) -C deps/mlsd clean
